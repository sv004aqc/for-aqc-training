import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CommonClass {
    public WebDriver webdriver;
    public WebDriverWait wait;

    public CommonClass(WebDriver driver){
    webdriver = driver;
    wait = new WebDriverWait(webdriver, 20);
    PageFactory.initElements(webdriver,this);
    }

    public void waitForEndOfAllAjaxes(){

        WebDriverWait wait = new WebDriverWait(webdriver, 20);

        wait.until(new ExpectedCondition<Boolean>() {

            @Override

            public Boolean apply(org.openqa.selenium.WebDriver driver) {

                return (Boolean)((JavascriptExecutor)driver).executeScript("return jQuery.active == 0");

            }

        });
    }
}
