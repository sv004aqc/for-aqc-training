import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class ConstitutionTextPage extends CommonClass {
    /**
     * Text of document of "Constitution"
     */
    @FindBy (id = "docText")
    private WebElement documentText;

    /**
     * Constructor of class ConstitutionTextPage
     * @param driver
     */
    public ConstitutionTextPage(WebDriver driver){
        super(driver);
        wait.until(ExpectedConditions.visibilityOf(documentText));
        try
        {
            /* Using Thread.sleep() we can add delay in our application in a millisecond time,
               that allows to see the WebElement.
            */
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
        }
    }

    /**
     * Method allows to get text from the title of current page without reference to size of letters
     * @return
     */
    public String getTextForAssert(){
        return webdriver.getTitle().toLowerCase();
    }
}
