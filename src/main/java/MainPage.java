import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class MainPage extends CommonClass{

    private String url = "http://www.codex.in.ua/";
    /**
     * SearchBar - WebElement on Main page
     */
    @FindBy (id = "search")
    public WebElement searchBar;
    /**
     * SearchButton - WebElement on Main page
     */
    @FindBy (xpath = "//*[@id='searchButton']/i")
    private WebElement searchButton;
    /**
     * Link of section named "Конституція України" on Main page
     */
    @FindBy (xpath = "//li[5]")
    public WebElement sectionLink;

    /**
     * Constructor of class MainPage
     * @param driver
     */
    public MainPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Method allows to open Main page of site in browser by entering it's URL to address bar.
     * Method waits for loading the link of section named "Конституція України".
     */
    public void openMainPage(){
        webdriver.get(url);
        wait.until(ExpectedConditions.visibilityOf(sectionLink));
    }

    /**
     * Method allows to type request into the SearchBar for the nest search.
     * @param searchRequest
     */
    public void typingSearchRequest(String searchRequest){
        searchBar.sendKeys(searchRequest);
    }

    /**
     * Method allows to click on SearchButton and redirect to the next page (get search results).
     * @return
     */
    public ResultsPage getSearchResult(){
        searchButton.click();
        return new ResultsPage(webdriver);
    }
}
