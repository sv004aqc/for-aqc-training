import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.ui.ExpectedConditions;


public class ResultsPage extends CommonClass {

    /**
     * Link of first section named "Конституция Украины" on Results page
     */
    @FindBy (xpath = "//h6[@class='ng-scope']/a[contains(text(),'Конституция Украины ')]")
    private WebElement firstLink;

    /**
     * Constructor of class ResultsPage
     * @param driver
     */
    public ResultsPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.visibilityOf(firstLink));
    }

    /**
     * Method allows to click on the link of first section named "Конституция Украины" and redirect to the ConstitutionTextPage
     * @return
     */
    public ConstitutionTextPage openSearchResult(){
        waitForEndOfAllAjaxes();
        firstLink.click();
        return new ConstitutionTextPage(webdriver);
    }
}
