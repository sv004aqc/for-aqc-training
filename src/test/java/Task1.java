import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class Task1 {
    private MainPage mainCodexPage;
    public WebDriver driver;
    private static final String REQUEST_FOR_SEARCH = "конституция ";

    @Test
    public void Test1(){
        driver = new FirefoxDriver();
        mainCodexPage = new MainPage(driver);
        mainCodexPage.openMainPage();
        mainCodexPage.typingSearchRequest(REQUEST_FOR_SEARCH);
        ResultsPage resultsPage = mainCodexPage.getSearchResult();
        ConstitutionTextPage constitutionText = resultsPage.openSearchResult();
        String textForAssert = constitutionText.getTextForAssert();
        System.out.println(REQUEST_FOR_SEARCH);
        System.out.println(textForAssert);
        assertTrue(textForAssert.contains(REQUEST_FOR_SEARCH), "\nTitle not contains the word " + REQUEST_FOR_SEARCH + ".");

    }


    @AfterClass
    public void closeBrowser() {
        if (driver != null) {
            driver.quit();
        }
    }
}